// Copyright 2012 The KrakenCode Authors. All rights reserved.
// Use of this source code is governed by a BSD-style
// license that can be found in the LICENSE file.

package main

import (
	"fmt"
)

const (
	colWhite string = "white"
	colRed          = "red"
	colGreen        = "green"
)

var (
	errFailedLocal = fmt.Errorf("unable to get local version")
)

// GoUpdate (bitbar)
// ============================================================================
// A special verson of goupdate to be a bitbar plugin.
// =============================================================================
// Copyright 2012 The KrakenCode Authors. All rights reserved.
// Use of this source code is governed by a BSD-style
// license that can be found in the LICENSE file.

package main

import (
	"fmt"
	"runtime"

	gdata "bitbucket.org/krakencode/golangpkg/golangdldata"
	gver "bitbucket.org/krakencode/golangpkg/golangversion"
)

func main() {
	// var err error
	icon := StatIconUpToDate
	curVer, err := getRemoteVersion()
	locVer := gver.NewFromInstalled("/usr/local/go/bin/go")
	curColor := colWhite
	locColor := colWhite

	if err != nil {
		icon = StatIconError
		curColor = colRed
	} else if locVer.Equals(gver.GoVersion{}) {
		icon = StatIconError
		err = errFailedLocal
		locColor = colRed
	} else if curVer.IsNewer(*locVer) {
		icon = StatIconNeedUpdate
		curColor = colGreen
	}

	fmt.Printf("|image=%s\n", icon)

	if err != nil {
		fmt.Println("---")
		fmt.Println("Error: " + err.Error())
	}

	fmt.Println("---")
	fmt.Printf("Current: %s|color=%s\n", curVer.String(), curColor)
	fmt.Printf("Installed: %s|color=%s\n", locVer.String(), locColor)

}

func getRemoteVersion() (*gver.GoVersion, error) {

	// Get data from remote site
	gd := gdata.New()
	err := gd.Populate(gdata.DLSiteURL)
	if err != nil {
		return &gver.GoVersion{}, err
	}

	// Filter down the data
	gd.FilterStatus(gdata.StatusStable).
		FilterOS(gdata.OsMacOs).
		FilterArch(gdata.ArchFromString(runtime.GOARCH)).
		FilterKind(gdata.KindArchive)

	// Pull lattest
	for _, ver := range gd.Versions[gd.MaxVersion()] {
		return &ver.Version, nil
	}

	return &gver.GoVersion{}, fmt.Errorf("Unable to find version")
}
